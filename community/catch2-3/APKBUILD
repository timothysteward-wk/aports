# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer:
pkgname=catch2-3
pkgver=3.5.4
pkgrel=0
arch="all"
url="https://github.com/catchorg/Catch2"
pkgdesc="Modern, C++-native, header-only, test framework for unit-tests (v3)"
license="BSL-1.0"
makedepends="
	cmake
	python3
	samurai
	"
source="https://github.com/catchorg/Catch2/archive/v$pkgver/catch2-v$pkgver.tar.gz"
subpackages="$pkgname-doc"
builddir="$srcdir/Catch2-$pkgver"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# ApprovalTests is broken https://github.com/catchorg/Catch2/issues/1780
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "ApprovalTests"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c22ad6a2fbf8665b8775d72dcdc6bfde324eb224fcd897ebce5e62c7ac7640823550198fff45e1ea548a5923db4392ce7009ff784ef78bd59356a2aae5337976  catch2-v3.5.4.tar.gz
"
